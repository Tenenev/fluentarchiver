﻿namespace FluentArchiver.Console
{
    using System;
    using Autofac;
    using Services.Contracts;
    using Domain.Enums;
    using Services.Implementations;

    class Program
    {
        private static ICancellable _currentService;

        static int Main(string[] args)
        {
            try
            {
                Console.CancelKeyPress += ConsoleOnCancelKeyPress;

                var startTime = DateTime.Now;
                Console.WriteLine($"Started at {startTime.ToLongTimeString()}");

                var container = GetContainer();

                var configurationService = container.Resolve<IConfigurationService>();

                var archivationParameters = configurationService.GetArchivationParameters(args);

                switch (archivationParameters.ArchivationMode)
                {
                    case ArchivationMode.Compress:
                        var compressorService = container.Resolve<ICompressorService>();
                        _currentService = compressorService;
                        compressorService.Start(archivationParameters.InputFilePath, archivationParameters.OutputFilePath);
                        break;
                    case ArchivationMode.Decompress:
                        var decompressorService = container.Resolve<IDecompressorService>();
                        _currentService = decompressorService;
                        decompressorService.Start(archivationParameters.InputFilePath, archivationParameters.OutputFilePath);
                        break;
                }

                var endTime = DateTime.Now;
                var processingTime = endTime - startTime;
                Console.WriteLine($"Completed at {endTime.ToLongTimeString()}");
                Console.WriteLine($"Duration: {processingTime.Minutes:00}:{processingTime.Seconds:00}");
            }
            catch(Exception exc)
            {
                Console.WriteLine(exc.Message);
                return 1;
            }
            
            return 0;
        }

        private static void ConsoleOnCancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            Console.WriteLine("\nCancelling...");
            e.Cancel = true;
            _currentService.Cancel();
        }

        private static IContainer GetContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ConfigurationService>().As<IConfigurationService>();
            builder.RegisterType<ConsoleProgressBarService>().As<IProgressBarService>().SingleInstance();
            builder.RegisterGeneric(typeof(MultithreadQueueService<>)).As(typeof(IMultithreadQueueService<>));
            builder.RegisterType<CompressorService>().As<ICompressorService>();
            builder.RegisterType<DecompressorService>().As<IDecompressorService>();

            var container = builder.Build();
            return container;
        }
    }
}
