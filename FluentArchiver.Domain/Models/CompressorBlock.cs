﻿namespace FluentArchiver.Domain.Models
{
    public class CompressorBlock : BaseBlock
    {
        public byte[] Bytes { get; set; }

        public byte[] CompressedBytes { get; set; }
    }
}
