﻿using FluentArchiver.Domain.Enums;

namespace FluentArchiver.Domain.Models
{
    public class ArchivationParameters
    {
        public ArchivationMode ArchivationMode { get; set; }

        public string InputFilePath { get; set; }

        public string OutputFilePath { get; set; }
    }
}
