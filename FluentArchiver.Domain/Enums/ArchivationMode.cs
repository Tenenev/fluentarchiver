﻿namespace FluentArchiver.Domain.Enums
{
    public enum ArchivationMode
    {
        Compress,
        Decompress
    }
}
