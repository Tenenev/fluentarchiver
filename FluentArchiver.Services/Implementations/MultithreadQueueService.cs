﻿using System;
using System.Collections.Generic;
using System.Threading;
using FluentArchiver.Domain.Models;
using FluentArchiver.Services.Contracts;

namespace FluentArchiver.Services.Implementations
{
    public class MultithreadQueueService<T> : IMultithreadQueueService<T> where T : BaseBlock
    {
        private readonly object _locker = new object();
        private readonly Queue<T> _queue = new Queue<T>();
        private bool _isStopped;
        private int _lastId;

        public void Enqueue(T item)
        {
            lock (_locker)
            {
                if (_isStopped)
                {
                    throw new InvalidOperationException("Queue already stopped");
                }

                item.Id = _lastId;
                _queue.Enqueue(item);
                _lastId++;
                Monitor.PulseAll(_locker);
            }
        }

        public void OrderedEnqueue(T item)
        {
            var id = item.Id;
            lock (_locker)
            {
                if (_isStopped)
                {
                    throw new InvalidOperationException("Queue already stopped");
                }

                while (id != _lastId)
                {
                    Monitor.Wait(_locker);
                }

                _queue.Enqueue(item);
                _lastId++;
                Monitor.PulseAll(_locker);
            }
        }

        public T Dequeue()
        {
            lock (_locker)
            {
                while (_queue.Count == 0 && !_isStopped)
                {
                    Monitor.Wait(_locker);
                }

                if (_queue.Count == 0)
                {
                    return null;
                }

                return _queue.Dequeue();
            }
        }

        public void Stop()
        {
            lock (_locker)
            {
                _isStopped = true;
                Monitor.PulseAll(_locker);
            }
        }

        public void Clear()
        {
            lock (_locker)
            {
                _queue.Clear();
            }
        }
    }
}
