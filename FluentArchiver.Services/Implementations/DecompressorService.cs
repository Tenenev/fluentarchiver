﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using FluentArchiver.Domain.Models;
using FluentArchiver.Services.Contracts;

namespace FluentArchiver.Services.Implementations
{
    public class DecompressorService : IDecompressorService
    {
        private const int BufferSize = 1024 * 1024;
        private static readonly int ThreadsCount = Environment.ProcessorCount;

        private readonly object _progressLocker = new object();
        private long _totalBytes = 0L;
        private long _bytesProcessed = 0L;

        private string _inputFilePath;
        private string _outputFilePath;

        private readonly IMultithreadQueueService<CompressorBlock> _decompressionQueue;
        private readonly IMultithreadQueueService<CompressorBlock> _writeQueue;
        private readonly IProgressBarService _progressBarService;

        private readonly ManualResetEvent[] _decompressionCompletedEvents = new ManualResetEvent[ThreadsCount];

        private bool _isCancelled = false;

        public DecompressorService(IMultithreadQueueService<CompressorBlock> decompressionQueue,
            IMultithreadQueueService<CompressorBlock> writeQueue, IProgressBarService progressBarService)
        {
            this._decompressionQueue = decompressionQueue;
            this._writeQueue = writeQueue;
            this._progressBarService = progressBarService;
        }

        public void Start(string inputFilePath, string outputFilePath)
        {
            _progressBarService.Reset();

            this._inputFilePath = inputFilePath;
            this._outputFilePath = outputFilePath;

            var readerThread = new Thread(Read);
            readerThread.Start();

            for (var i = 0; i < ThreadsCount; i++)
            {
                var threadNum = i;
                _decompressionCompletedEvents[i] = new ManualResetEvent(false);
                var compressorThread = new Thread(() => Decompress(threadNum));
                compressorThread.Start();
            }

            var writerThread = new Thread(Write);
            writerThread.Start();

            WaitHandle.WaitAll(_decompressionCompletedEvents);
            _writeQueue.Stop();

            writerThread.Join();

            if (_isCancelled)
            {
                throw new ApplicationException("Program execution was aborted");
            }
        }

        public void Cancel()
        {
            _isCancelled = true;
        }

        private void Read()
        {
            try
            {
                var fileInfo = new FileInfo(_inputFilePath);
                lock (_progressLocker)
                {
                    _totalBytes = fileInfo.Length;
                }

                using (var inputFileStream = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    while (inputFileStream.Position < inputFileStream.Length && !_isCancelled)
                    {
                        var compressedBufferSizeBlock = new byte[4];

                        if (inputFileStream.Read(compressedBufferSizeBlock, 0, compressedBufferSizeBlock.Length) == 0)
                        {
                            throw new FileLoadException("Compressed file is broken");
                        }

                        var compressedCount = BitConverter.ToInt32(compressedBufferSizeBlock, 0);
                        var compressedBuffer = new byte[compressedCount];
                        if (inputFileStream.Read(compressedBuffer, 0, compressedBuffer.Length) == 0)
                        {
                            throw new FileLoadException("Compressed file is broken");
                        }

                        _decompressionQueue.Enqueue(new CompressorBlock { CompressedBytes = compressedBuffer });
                    }

                    _decompressionQueue.Stop();
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("\n" + exc.Message);
                _isCancelled = true;
            }
        }

        private void Decompress(int threadNum)
        {
            try
            {
                while (!_isCancelled)
                {
                    var block = _decompressionQueue.Dequeue();

                    if (block == null)
                    {
                        return;
                    }

                    var buffer = new byte[BufferSize];
                    int count;

                    using (var inputMemoryStream = new MemoryStream(block.CompressedBytes))
                    using (var decompressionStream = new GZipStream(inputMemoryStream, CompressionMode.Decompress))
                    {
                        count = decompressionStream.Read(buffer, 0, buffer.Length);
                    }
                    block.Bytes = buffer.Take(count).ToArray();
                    
                    lock (_progressLocker)
                    {
                        _bytesProcessed += block.CompressedBytes.Length;
                        var progressPercent = (double)_bytesProcessed / _totalBytes * 100;
                        _progressBarService.SetProgress(progressPercent);
                    }

                    _writeQueue.OrderedEnqueue(block);
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("\n" + exc.Message);
                _isCancelled = true;
            }
            finally
            {
                _decompressionCompletedEvents[threadNum].Set();
            }
        }

        private void Write()
        {
            try
            {
                using (var outputFileStream = File.Open(_outputFilePath, FileMode.Create, FileAccess.Write, FileShare.Write))
                {
                    while (!_isCancelled)
                    {
                        var block = _writeQueue.Dequeue();

                        if (block == null)
                        {
                            return;
                        }

                        outputFileStream.Write(block.Bytes, 0, block.Bytes.Length);
                    }
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("\n" + exc.Message);
                _isCancelled = true;
            }
        }
    }
}
