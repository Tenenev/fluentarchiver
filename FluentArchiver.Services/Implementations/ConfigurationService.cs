﻿using System;
using System.IO;
using System.Linq;
using FluentArchiver.Domain.Enums;
using FluentArchiver.Domain.Models;
using FluentArchiver.Services.Contracts;

namespace FluentArchiver.Services.Implementations
{
    public class ConfigurationService : IConfigurationService
    {
        private static readonly string[] validCommands = { "compress", "decompress" };

        public ArchivationParameters GetArchivationParameters(string[] args)
        {
            if (args.Length != 3)
            {
                throw new ArgumentException("Unsupported parameters. Please use the following pattern:\n compress/decompress [Source file] [Destination file].");
            }

            if (validCommands.All(c => !c.Equals(args[0], StringComparison.InvariantCultureIgnoreCase)))
            {
                throw new ArgumentException("Unable to recognize the command. Please use the following pattern:\n compress/decompress [Source file] [Destination file].");
            }

            if (string.Equals(args[1], args[2], StringComparison.InvariantCultureIgnoreCase))
            {
                throw new ArgumentException("Source and destination files shall be different.");
            }

            if (!File.Exists(args[1]))
            {
                throw new FileNotFoundException($"Source file '{args[1]}' not exists");
            }

            var archivationMode = (ArchivationMode) Enum.Parse(typeof(ArchivationMode), args[0], true);
            var inputFilePath = args[1];
            var outputFilePath = args[2];

            var archivationParameters = new ArchivationParameters
            {
                ArchivationMode = archivationMode,
                InputFilePath = inputFilePath,
                OutputFilePath = outputFilePath
            };

            return archivationParameters;
        }
    }
}
