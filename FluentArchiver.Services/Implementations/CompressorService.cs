﻿using System;
using System.IO;
using System.IO.Compression;
using System.Threading;
using FluentArchiver.Domain.Models;
using FluentArchiver.Services.Contracts;

namespace FluentArchiver.Services.Implementations
{
    public class CompressorService : ICompressorService
    {
        private const int BufferSize = 1024 * 1024;
        private static readonly int ThreadsCount = Environment.ProcessorCount;

        private readonly object _progressLocker = new object();
        private long _totalBytes = 0L;
        private long _bytesProcessed = 0L;

        private string _inputFilePath;
        private string _outputFilePath;

        private readonly IMultithreadQueueService<CompressorBlock> _compressionQueue;
        private readonly IMultithreadQueueService<CompressorBlock> _writeQueue;
        private readonly IProgressBarService _progressBarService;

        private readonly ManualResetEvent[] _compressionCompletedEvents = new ManualResetEvent[ThreadsCount];

        private bool _isCancelled = false;

        public CompressorService(IMultithreadQueueService<CompressorBlock> compressionQueue,
            IMultithreadQueueService<CompressorBlock> writeQueue, IProgressBarService progressBarService)
        {
            this._compressionQueue = compressionQueue;
            this._writeQueue = writeQueue;
            this._progressBarService = progressBarService;
        }

        public void Start(string inputFilePath, string outputFilePath)
        {
            _progressBarService.Reset();

            this._inputFilePath = inputFilePath;
            this._outputFilePath = outputFilePath;

            var readerThread = new Thread(Read);
            readerThread.Start();

            for (var i = 0; i < ThreadsCount; i++)
            {
                var threadNum = i;
                _compressionCompletedEvents[i] = new ManualResetEvent(false);
                var compressorThread = new Thread(() => Compress(threadNum));
                compressorThread.Start();
            }
            
            var writerThread = new Thread(Write);
            writerThread.Start();
            
            WaitHandle.WaitAll(_compressionCompletedEvents);
            _writeQueue.Stop();

            writerThread.Join();

            if (_isCancelled)
            {
                throw new ApplicationException("Program execution was aborted");
            }
        }

        public void Cancel()
        {
            _isCancelled = true;
        }

        private void Read()
        {
            try
            {
                var fileInfo = new FileInfo(_inputFilePath);
                lock (_progressLocker)
                {
                    _totalBytes = fileInfo.Length;
                }

                using (var inputFileStream = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    while (inputFileStream.Position < inputFileStream.Length && !_isCancelled)
                    {
                        int currentBytesCount;
                        if (inputFileStream.Length - inputFileStream.Position < BufferSize)
                        {
                            currentBytesCount = (int) (inputFileStream.Length - inputFileStream.Position);
                        }
                        else
                        {
                            currentBytesCount = BufferSize;
                        }

                        var currentBuffer = new byte[currentBytesCount];
                        inputFileStream.Read(currentBuffer, 0, currentBytesCount);

                        _compressionQueue.Enqueue(new CompressorBlock {Bytes = currentBuffer});
                    }

                    _compressionQueue.Stop();
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("\n" + exc.Message);
                _isCancelled = true;
            }
        }

        private void Compress(int threadNum)
        {
            try
            {
                while (!_isCancelled)
                {
                    var block = _compressionQueue.Dequeue();

                    if (block == null)
                    {
                        return;
                    }

                    byte[] compressedBuffer;

                    using (var outputMemoryStream = new MemoryStream())
                    {
                        using (var compressionStream = new GZipStream(outputMemoryStream, CompressionMode.Compress))
                        {
                            compressionStream.Write(block.Bytes, 0, block.Bytes.Length);
                        }

                        compressedBuffer = outputMemoryStream.ToArray();
                    }

                    var compressedBufferWithLength = new byte[compressedBuffer.Length + 4];
                    BitConverter.GetBytes(compressedBuffer.Length).CopyTo(compressedBufferWithLength, 0);
                    compressedBuffer.CopyTo(compressedBufferWithLength, 4);
                    block.CompressedBytes = compressedBufferWithLength;
                    
                    lock (_progressLocker)
                    {
                        _bytesProcessed += block.Bytes.Length;
                        var progressPercent = (double)_bytesProcessed / _totalBytes * 100;
                        _progressBarService.SetProgress(progressPercent);
                    }

                    _writeQueue.OrderedEnqueue(block);
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("\n" + exc.Message);
                _isCancelled = true;
            }
            finally
            {
                _compressionCompletedEvents[threadNum].Set();
            }
        }

        private void Write()
        {
            try
            {
                using (var outputFileStream =
                    File.Open(_outputFilePath, FileMode.Create, FileAccess.Write, FileShare.Write))
                {
                    while (!_isCancelled)
                    {
                        var block = _writeQueue.Dequeue();

                        if (block == null)
                        {
                            return;
                        }

                        outputFileStream.Write(block.CompressedBytes, 0, block.CompressedBytes.Length);
                    }
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("\n" + exc.Message);
                _isCancelled = true;
            }
        }
    }
}
