﻿using System;
using System.Linq;
using FluentArchiver.Services.Contracts;

namespace FluentArchiver.Services.Implementations
{
    public class ConsoleProgressBarService : IProgressBarService
    {
        private const int consoleLineLength = 80;

        private int currentProgress = 0;

        public void Reset()
        {
            currentProgress = 0;
        }

        public void SetProgress(double progressPercent)
        {
            var progress = (int)Math.Ceiling(progressPercent / 100 * consoleLineLength);

            if (progress > currentProgress)
            {
                var progressDiff = progress - currentProgress;
                var symbolsToAdd = new string(new char[progressDiff].Select(s => '*').ToArray());
                if(progress == consoleLineLength)
                {
                    symbolsToAdd += "\n";
                }
                Console.Write(symbolsToAdd);
                currentProgress = progress;
            }
        }
    }
}
