﻿namespace FluentArchiver.Services.Contracts
{
    public interface ICancellable
    {
        void Cancel();
    }
}
