﻿namespace FluentArchiver.Services.Contracts
{
    public interface ICompressorService : ICancellable
    {
        void Start(string inputFilePath, string outputFilePath);
    }
}
