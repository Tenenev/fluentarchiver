﻿using FluentArchiver.Domain.Models;

namespace FluentArchiver.Services.Contracts
{
    public interface IConfigurationService
    {
        ArchivationParameters GetArchivationParameters(string[] args);
    }
}
