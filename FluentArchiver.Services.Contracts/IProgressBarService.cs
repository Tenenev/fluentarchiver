﻿namespace FluentArchiver.Services.Contracts
{
    public interface IProgressBarService
    {
        void SetProgress(double progressPercent);

        void Reset();
    }
}
