﻿namespace FluentArchiver.Services.Contracts
{
    public interface IDecompressorService : ICancellable
    {
        void Start(string inputFilePath, string outputFilePath);
    }
}
