﻿using FluentArchiver.Domain.Models;

namespace FluentArchiver.Services.Contracts
{
    public interface IMultithreadQueueService<T> where T : BaseBlock
    {
        void Enqueue(T item);

        void OrderedEnqueue(T item);

        T Dequeue();

        void Stop();

        void Clear();
    }
}
